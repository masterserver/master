﻿namespace Master_Server.Configs;

public class ModuleConfig
{
    public string ModulesFolder = "Plugins";
    public string DependenciesModuleFolder = "Dependencies";
}