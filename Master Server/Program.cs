using System.Net;
using Darari.Server.Abstractions;
using Master_Server;
using Master_Server.Extentions;

/* Register modules */

var connectionManager = new ConnectionManager();
var modules = new ModulesManager();

modules.InitializeModules(connectionManager);

/* Create builder app */
var builder = WebApplication.CreateBuilder(args);
builder.WebHost.UseUrls("http://localhost:6969");

/* Register databases from modules */
/* Providers add databases to the builder via AddDbContext(The extension method from EntityFrameworkCore), so you need to call them before builder.Build */
var databaseRegister = builder.RegisterDatabasesFromModules(modules);

/* Build the application */
var app = builder.Build();

/* Register modules dependencies */
connectionManager.InitLifeToken(app.Lifetime.ApplicationStopping);
modules.InitLifeToken(app.Lifetime.ApplicationStopping);
databaseRegister.InitServiceProvider(app.Services);

databaseRegister.InitDatabaseDispatcherToModules(modules);

/* Register App */

var wsOptions = new WebSocketOptions()
{
    KeepAliveInterval = TimeSpan.FromMinutes(2)
};

app.UseWebSockets(wsOptions);

app.Map("/ws", async context =>
{
    if (context.WebSockets.IsWebSocketRequest)
    {
        using var webSocket = await context.WebSockets.AcceptWebSocketAsync();
        
        var client = connectionManager.RegisterConnection(webSocket, context.Connection.RemoteIpAddress);

        while (client.State != ConnectionState.Disconnected)
        {
            //Спасибо мелкомягким за причину костылить
        }
    }
    else
    {
        context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
    }
});

//app.UseHttpsRedirection();

app.Run();