﻿using Darari.Server.Abstractions.EntityFrameworkUtils;

namespace Master_Server.Extentions;

public static class DatabaseExtensions
{
    public static DatabasesRegister RegisterDatabasesFromModules(this WebApplicationBuilder builder, ModulesManager modules)
    {
        var databaseRegister = new DatabasesRegister(builder.Configuration, builder.Services);

        var databasesProviders = modules.GetModulesOfType<IDatabaseProvider>();
        for (int i = 0; i < databasesProviders.Count; i++)
        {
            databasesProviders[i].RegisterDatabase(databaseRegister);
        }

        return databaseRegister;
    }

    public static void InitDatabaseDispatcherToModules(this IDatabaseDispatcher dispatcher, ModulesManager modules)
    {
        var databasesProcessors = modules.GetModulesOfType<IDatabaseProcessor>();
        for (int i = 0; i < databasesProcessors.Count; i++)
        {
            databasesProcessors[i].SetDispatcher(dispatcher);
        }
    }
}