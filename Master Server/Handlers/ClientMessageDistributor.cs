﻿using Darari.Serialization.Abstractions;
using Darari.Serialization.Entities;
using Darari.Serialization.Extensions;
using Darari.Server.Abstractions;


namespace Master_Server.Handlers;

public class ClientMessageDistributor: IMessageDistributor
{
    class PackageHandlerData
    {
        public Dictionary<uint, Action<IClient, INetworkReader, uint>> PackageData;
    }

    private Dictionary<uint, PackageHandlerData> _handlers = new Dictionary<uint, PackageHandlerData>();
    private Dictionary<Type, IWrap> _wrapers = new Dictionary<Type, IWrap>();

    internal void UnpackMessage(IClient client, ArraySegment<byte> segment)
    {
        var reader = NetworkReaderPool.Get(segment);
        
        var moduleId = reader.ReadUInt();
        var packageId = reader.ReadUInt();

        if (_handlers.TryGetValue(moduleId, out var packageData))
        {
            packageData.PackageData[packageId].Invoke(client, reader, packageId);
        }
        
        if(reader is IDisposable disposable) disposable.Dispose();
    }
    
    public void RegisterHandler<T>(uint moduleId, uint messageId, Action<IClient, uint, T> callback) where T: ISerializable
    {
        if (!_wrapers.ContainsKey(typeof(T)))
        {
            _wrapers.Add(typeof(T), new WrapData<T>());
        }
        
        AddPackage(moduleId, messageId, callback);
    }

    public void RegisterHandlerUnmanaged<T>(uint handlerId, uint messageId, Action<IClient, uint, T> callback) where T : unmanaged
    {
        if (_wrapers.ContainsKey(typeof(T)))
        {
            _wrapers.Add(typeof(T), new UnmanagedWrapData<T>());
        }
        
        AddPackageUnmanaged(handlerId, messageId, callback);
    }

    public void RemoveMessageFromHandler(uint handlerId, uint messageId)
    {
        if (_handlers.TryGetValue(handlerId, out var packageData))
        {
            packageData.PackageData.Remove(messageId);
        }
    }

    public void RemoveHandler(uint handlerId)
    {
        _handlers.Remove(handlerId);
    }
    
    private void AddPackage<T>(uint handlerId, uint messageId, Action<IClient, uint, T> callback) where T: ISerializable
    {
        if (!_handlers.TryGetValue(handlerId, out var dictionary))
        {
            dictionary = new PackageHandlerData()
            {
                PackageData = new Dictionary<uint, Action<IClient, INetworkReader, uint>>(),
            };
            
            _handlers.Add(handlerId, dictionary);
        }
        
        
        dictionary.PackageData.Add(messageId, (connection, reader, packageId) 
            => (_wrapers[typeof(T)] as WrapData<T>).Wrap(handlerId, connection, packageId, reader, callback));
    }

    private void AddPackageUnmanaged<T>(uint handlerId, uint messageId, Action<IClient, uint, T> callback) where T : unmanaged
    {
        if (!_handlers.TryGetValue(handlerId, out var packageHandler))
        {
            packageHandler = new PackageHandlerData()
            {
                PackageData = new Dictionary<uint, Action<IClient, INetworkReader, uint>>(),
            };
            
            _handlers.Add(handlerId, packageHandler);
        }
        
        packageHandler.PackageData.Add(messageId, (connection, reader, packageId)
            => (_wrapers[typeof(T)] as UnmanagedWrapData<T>).Wrap(handlerId, connection, packageId, reader, callback));
    }
}