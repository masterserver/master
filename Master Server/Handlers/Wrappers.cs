﻿using Darari.Serialization.Abstractions;
using Darari.Server.Abstractions;


namespace Master_Server.Handlers;

interface IWrap
{
    public Type Type { get; }
}
    
abstract class WrapDataBase<T>: IWrap
{
    public Type Type => typeof(T);

    public void Wrap(uint handlerId, IClient client, uint packageId, INetworkReader reader, Action<IClient, uint, T> callback)
    {
        var result = UnpackInternal(reader);
        callback.Invoke(client, packageId, result);
    }

    protected abstract T UnpackInternal(INetworkReader reader);
}

class WrapData<T>: WrapDataBase<T> where T: ISerializable
{
    protected override T UnpackInternal(INetworkReader reader)
    {
        var result = Activator.CreateInstance<T>();
        result.Deserialize(reader);
        return result;
    }
}
    
class UnmanagedWrapData<T>: WrapDataBase<T> where T: unmanaged
{
    protected override T UnpackInternal(INetworkReader reader)
    {
        return reader.ReadBlittable<T>();
    }
}