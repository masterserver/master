﻿using System.Net;
using System.Net.WebSockets;
using Darari.Serialization.Abstractions;
using Darari.Serialization.Entities;
using Darari.Serialization.Extensions;
using Darari.Server.Abstractions;


namespace Master_Server.Entities.Client;

public class ClientConnection: IClient
{
    private readonly Queue<ArraySegment<byte>> _messagesQueue = new Queue<ArraySegment<byte>>();

    internal Queue<ArraySegment<byte>> Messages
    {
        get
        {
            lock (_messagesQueue)
            {
                return _messagesQueue;
            }
        }
    }
    
    internal WebSocket WebSocket { get; }

    public uint ID { get; }
    
    public IPAddress? Address { get; }

    public ConnectionState State
    {
        get
        {
            return WebSocket.State switch
            {
                WebSocketState.Connecting => ConnectionState.Connecting,
                WebSocketState.Open => ConnectionState.Connected,
                WebSocketState.CloseReceived or WebSocketState.CloseSent => ConnectionState.Disconnecting,
                WebSocketState.Aborted or WebSocketState.Closed => ConnectionState.Disconnected,
                _ => ConnectionState.None
            };
        }
    }


    public ClientConnection(uint id, IPAddress? address, WebSocket socket)
    {
        ID = id;
        Address = address;
        WebSocket = socket;
    }
    
    public void Send<T>(uint moduleId, uint packageId, T message)
    {
        var writer = NetworkWriterPool.Get();

        writer.WriteUInt(moduleId);
        writer.WriteUInt(packageId);

        if (message is ISerializable serializable)
        {
            serializable.Serialize(writer);
        }

        lock (_messagesQueue)
        {
            _messagesQueue.Enqueue(writer.ToArraySegment());
        }
        
        if(writer is IDisposable disposable) disposable.Dispose();
    }

    public void Disconnect(string reason) => Disconnect(reason, WebSocketCloseStatus.NormalClosure);

    internal void Disconnect(string reason, WebSocketCloseStatus status)
    {
        WebSocket.CloseAsync(status, string.IsNullOrWhiteSpace(reason) ? "Client has been disconnected from server" : reason, CancellationToken.None);
        WebSocket.Dispose();
    }

    internal void Send(ArraySegment<byte> segment, CancellationToken token, WebSocketMessageType type = WebSocketMessageType.Binary, WebSocketMessageFlags flags = WebSocketMessageFlags.EndOfMessage)
    {
        WebSocket.SendAsync(segment, type, flags, token);
    }
}