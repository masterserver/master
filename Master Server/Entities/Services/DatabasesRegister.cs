﻿using Darari.Server.Abstractions.EntityFrameworkUtils;
using Microsoft.EntityFrameworkCore;
//using Pomelo.EntityFrameworkCore.MySql.Infrastructure;

namespace Master_Server;

public class DatabasesRegister: IDatabaseDispatcher, IDatabaseVisitor
{
    private IServiceCollection _collection;
    private ConfigurationManager _configurationManager;
    private IServiceProvider? _serviceProvider;
    
    public DatabasesRegister(ConfigurationManager configurationManager, IServiceCollection collection)
    {
        _collection = collection;
        _configurationManager = configurationManager;
        _serviceProvider = null;
    }

    public void InitServiceProvider(IServiceProvider provider)
    {
        _serviceProvider = provider;
    }

    public void RegisterDatabase<T>() where T : DbContext
    {
        Console.WriteLine("Register database of type " + typeof(T).Name);
        _collection.AddDbContext<T>(options =>
        {
            
            options.UseSqlite(_configurationManager.GetConnectionString("Sqlite"));
           // options.UseMySql(_configurationManager.GetConnectionString("DefaultConnection"), ServerVersion.Create(0, 0, 1, ServerType.MariaDb),
           //     builder => { });
        });
    }

    public async void ProcessDatabase<T>(Func<T, Task> processWork) where T : DbContext
    {
        if (_serviceProvider == null)
        {
            return;
        }

        using var scope = _serviceProvider.CreateScope();
        await using var db = scope.ServiceProvider.GetService<T>();
        if (db != null)
        {
            await processWork.Invoke(db);
        }
    }
}