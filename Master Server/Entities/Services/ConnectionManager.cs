﻿using System.Net;
using System.Net.WebSockets;
using Darari.Server.Abstractions;
using Master_Server.Entities.Client;
using Master_Server.Handlers;

namespace Master_Server;

public class ConnectionManager: IConnectionsService
{
    public event Action<IClient> OnClientConnect = delegate {  };
    public event Action<IClient> OnClientDisconnect = delegate {  }; 

    public const int TickRate = 30;
    
    private uint _lastConnectionId;
    private List<ClientConnection> _connections = new List<ClientConnection>();
    private ClientMessageDistributor _messageDistributor = new ClientMessageDistributor();
    private ILogger _logger;

    private byte[] _buffer = new byte[1024 * 4];
    private CancellationToken _lifeCancellationTokenSource;
    private int _millisecondsAwait;

    public IMessageDistributor MessageDistributor => _messageDistributor;

    public IReadOnlyList<IClient> Connections => _connections;
    

    public ConnectionManager()
    {
        _lifeCancellationTokenSource = CancellationToken.None;
        _millisecondsAwait = (int)(1.0f / TickRate * 1000);
        _logger = LoggerFactory.Create(
            builder =>
            {
                builder.AddDebug();
                builder.AddSystemdConsole();
                builder.AddConsole();
            }).CreateLogger(nameof(ConnectionManager));

        Initialize();
    }

    public void InitLifeToken(CancellationToken token)
    {
        _lifeCancellationTokenSource = token;
    }

    public ClientConnection RegisterConnection(WebSocket webSocket, IPAddress? ip)
    {
        var id = ++_lastConnectionId;
        _logger.LogInformation($"A new user is connected. ID = {id}, IP = {ip?.ToString()}");
        var client = new ClientConnection(id, ip, webSocket);
        _connections.Add(client);
        OnClientConnect.Invoke(client);
        return client;
    }

    private async void Initialize()
    {
        await Tick();
    }

    private async Task Tick()
    {
        while (!_lifeCancellationTokenSource.IsCancellationRequested)
        {
            List<ClientConnection> disconnectedClients = new List<ClientConnection>();

            for (int i = 0; i < _connections.Count; i++)
            {
                if (!await TryProcessClient(_connections[i]))
                {
                    disconnectedClients.Add(_connections[i]);
                }
            }

            for (int i = 0; i < disconnectedClients.Count; i++)
            {
                OnClientDisconnect.Invoke(disconnectedClients[i]);
                _connections.Remove(disconnectedClients[i]);
            }

            await Task.Delay(_millisecondsAwait);
        }

        foreach (var connection in _connections)
        {
            OnClientDisconnect.Invoke(connection);
            connection.Disconnect(string.Empty);
        }
    }

    private async Task<bool> TryProcessClient(ClientConnection client)
    {
        WebSocketReceiveResult receiveResult = default;
        try
        {
            receiveResult = await client.WebSocket.ReceiveAsync(new ArraySegment<byte>(_buffer),
                _lifeCancellationTokenSource);
        }
        catch(WebSocketException)
        {
            _logger.LogInformation(client.ID + " closed the WebSocket connection without completing the close handshake");
            return false;
        }

        if (receiveResult.CloseStatus.HasValue)
        {
            _logger.LogInformation(client.ID + " disconnected, because " + receiveResult.CloseStatus.Value + ": " + receiveResult.CloseStatusDescription!);
            OnClientDisconnect.Invoke(client);
            client.Disconnect(receiveResult.CloseStatusDescription!, receiveResult.CloseStatus.Value);
            return false;
        }

        _messageDistributor.UnpackMessage(client, new ArraySegment<byte>(_buffer, 0, receiveResult.Count));

        while (client.Messages.Count > 0)
        {
            client.Send(client.Messages.Dequeue(), _lifeCancellationTokenSource);
        }
        
        return true;
    }

    public void Broadcast<T>(uint moduleId, uint messageId, T package)
    {
        for (int i = 0; i < _connections.Count; i++)
        {
            if (_connections[i].State == ConnectionState.Connected)
            {
                _connections[i].Send(moduleId, messageId, package);
            }
        }
    }
}