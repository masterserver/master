﻿using System.Reflection;
using System.Text.Json;
using Darari.Server.Abstractions;
using Master_Server.Configs;

namespace Master_Server;

public class ModulesManager: IModulesService
{
    public string ConfigsPath => Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Configs");
    public string CurrentConfigPath => Path.Combine(ConfigsPath, "Modules.json");
    
    private readonly List<ModuleBase> _modules = new List<ModuleBase>();
    private ModuleConfig? _config;
    
    private CancellationToken _lifeToken;

    public ModulesManager()
    {
        _lifeToken = CancellationToken.None;
        Directory.CreateDirectory(ConfigsPath);
        string json = "";
        if (!File.Exists(CurrentConfigPath))
        {
            _config = new ModuleConfig();
            json = JsonSerializer.Serialize(_config, JsonSerializerOptions.Default);
            File.WriteAllText(CurrentConfigPath, json);
            return;
        }
        json = File.ReadAllText(CurrentConfigPath);
        _config = JsonSerializer.Deserialize<ModuleConfig>(json);
    }

    public void InitLifeToken(CancellationToken token)
    {
        _lifeToken = token;
        _lifeToken.Register(OnDispose);
    }

    public void InitializeModules(ConnectionManager manager)
    {
        Console.WriteLine("Initialize internal services and modules");
        
        var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
        var modulesFullPath = Path.Combine(baseDirectory, _config!.ModulesFolder);

        if (!Directory.Exists(modulesFullPath))
        {
            Directory.CreateDirectory(modulesFullPath);
        }

        var modulesPaths = Directory.GetDirectories(modulesFullPath);

        foreach (var modulePath in modulesPaths)
        {
            InitializeModule(modulePath, modulesFullPath, manager);
        }
        
        Console.WriteLine("End initialized internal services and modules");
    }

    private void InitializeModule(string modulePath, string modulesFullPath, ConnectionManager manager)
    {
            var modulePathName = modulePath.Replace(modulesFullPath, string.Empty);
        
            InitializeDependencies(modulePath, modulePathName);

            foreach (var module in Directory.GetFiles(modulePath, "*.dll"))
            {
                var assembly = Assembly.LoadFrom(module);
                var handlers = assembly.GetTypes().Where(x => !x.IsAbstract && x.IsSubclassOf(typeof(ModuleBase)));

                foreach (var handler in handlers)
                {
                    CreateHandler(handler, manager);
                }
            }
    }

    private void InitializeDependencies(string modulePath, string moduleName)
    {
        var dependenciesPath = Path.Combine(modulePath, _config!.DependenciesModuleFolder);
        if (Directory.Exists(dependenciesPath))
        {
            Console.WriteLine($"Start initialize module from folder {moduleName}");
            List<string> dependenciesNames = new List<string>();
            foreach (var dependency in Directory.GetFiles(dependenciesPath, "*.dll"))
            {
                var dependencyAssembly = Assembly.LoadFrom(dependency);
                dependencyAssembly.EntryPoint?.Invoke(dependencyAssembly, null);
                    
                dependenciesNames.Add(dependencyAssembly.GetName().Name!);
            }
                
            Console.WriteLine($"Successfully load dependencies {string.Join(", ", dependenciesNames)} from folder {moduleName}");
        }
    }

    private void CreateHandler(Type handler, ConnectionManager manager)
    {
        try
        {
            var module = Activator.CreateInstance(handler, manager.MessageDistributor, manager, this) as ModuleBase;
            _modules.Add(module!);

            Console.Write("Success initialize ");
            var color = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(handler.Name);
            Console.ForegroundColor = color;
            Console.WriteLine(" module");
        }
        catch (Exception e)
        {
            Console.Write("Error when trying to initialize the module ");
            var color = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine(handler.Name);
            Console.WriteLine(e.ToString());
            Console.ForegroundColor = color;
        }
    }

    private void OnDispose()
    {
        foreach (var module in _modules)
        {
            if (module is IDisposable disposable)
            {
                disposable.Dispose();
            }
        }
        
        _modules.Clear();
    }

    public T? GetModule<T>() where T: ModuleBase
    {
        for (int i = 0; i < _modules.Count; i++)
        {
            if (_modules[i] is T resultModule)
            {
                return resultModule;
            }
        }

        return null;
    }

    public List<T> GetModulesOfType<T>()
    {
        var result = new List<T>();
        for (int i = 0; i < _modules.Count; i++)
        {
            if (_modules[i] is T value)
            {
                result.Add(value);
            }
        }

        return result;
    }
}